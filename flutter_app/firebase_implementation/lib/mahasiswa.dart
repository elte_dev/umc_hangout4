import 'package:firebase_database/firebase_database.dart';

class Mahasiswa {

  String _id;
  String _nama;

  Mahasiswa(this._id,this._nama);

  String get nama => _nama;

  String get id => _id;

  Mahasiswa.fromSnapshot(DataSnapshot snapshot) {
    _id = snapshot.key;
    _nama = snapshot.value['nama'];
  }
}