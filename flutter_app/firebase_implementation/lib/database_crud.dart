import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_implementation/mahasiswa.dart';

class DatabaseCrud {

  //A Firebase reference represents a particular location in your Database and
  // can be used for reading or writing data to that Database location.
  //This class is the starting point for all Database operations.
  // After you've initialized it with a URL, you can use it to read data, write data, and to create new DatabaseReferences.
  DatabaseReference _counterRef;
  DatabaseReference _userRef;

  //The subscription provides events to the listener, and holds the callbacks used to handle the events.
  // The subscription can also be used to unsubscribe from the events, or to temporarily pause the events from the stream.
  StreamSubscription<Event> _counterSubscription;
  StreamSubscription<Event> _messagesSubscription;

  //The entry point for accessing a Firebase Database.
  // You can get an instance by calling getInstance().
  // To access a location in the database and read or write data, use getReference()
  FirebaseDatabase database = new FirebaseDatabase();

  //increment variable x+=1
  int _counter;

  //Instances of DatabaseError are passed to callbacks when an operation failed.
  // They contain a description of the specific error that occurred.
  DatabaseError error;


  //create new instance based on its own class
  static final DatabaseCrud _instance = new DatabaseCrud.internal();

  DatabaseCrud.internal();

  //A factory interface that also reports the type of the created objects.
  //class contstructor
  factory DatabaseCrud() {
    return _instance;
  }

  //initState
  void initState() {

    //get database ref with child = counter
    //method 1 - Demonstrates configuring to the database using a file
    _counterRef = FirebaseDatabase.instance.reference().child('counter');

    //get database ref with child = user
    //method 2 - Demonstrates configuring the database directly
    _userRef = database.reference().child('user');

    //print value of key = counter
    database.reference().child('counter').once().then((DataSnapshot snapshot) {
      print('Connected to second database and read ${snapshot.value}');
    });

    //allow firebase to save data locally
    //automatically stores the data offline when there is no internet connection.
    // When the device connects to the internet, all the data will be pushed to the realtime database
    database.setPersistenceEnabled(true);
    database.setPersistenceCacheSizeBytes(10000000);
    _counterRef.keepSynced(true);
    _counterSubscription = _counterRef.onValue.listen((Event event) {
      error = null;
      _counter = event.snapshot.value ?? 0;
    }, onError: (Object o) {
      error = o;
    });
  }

  //getError
  DatabaseError getError() {
    return error;
  }

  //getCounter
  int getCounter() {
    return _counter;
  }

  //getAllUser
  DatabaseReference getUser() {
    return _userRef;
  }

  //deleteUser
  void deleteUser(Mahasiswa user) async {
    await _userRef.child(user.id).remove().then((_) {
      print('Transaction  committed.');
    });
  }

  //addUser
  addUser(Mahasiswa user) async {
    final TransactionResult transactionResult =
    await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userRef.push().set(<String, String>{
        "nama": "" + user.nama
      }).then((_) {
        print('Transaction  committed.');
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }

  //updateUser
  void updateUser(Mahasiswa user) async {
    await _userRef.child(user.id).update({
      "nama": "" + user.nama
    }).then((_) {
      print('Transaction  committed.');
    });
  }

  //dispose
  void dispose() {
    _messagesSubscription.cancel();
    _counterSubscription.cancel();
  }
}